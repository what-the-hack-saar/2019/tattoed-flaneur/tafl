# -*- coding: utf-8 -*-

"""Top-level package for Tattooed Flaneur command line tool."""

__author__ = """Achim Domma"""
__email__ = 'achim@domma.de'
__version__ = '0.1.0'
