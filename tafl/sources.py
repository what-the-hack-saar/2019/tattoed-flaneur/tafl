import feedparser
from lxml import etree
import requests

from tafl.core import source

@source
def flaneur():
    #   flaneur
    feed = feedparser.parse('http://www.der-flaneur.rocks/feeds/atom.xml')
    for e in feed.entries:
        yield e.link


@source
def tattoos():
    #   tattoo
    html = requests.get('https://tattoo.saarland/job_listing-sitemap.xml')
    tree = etree.fromstring(html.content)

    for x in tree.xpath('//*[local-name() = "loc"]'):
        url = x.text
        if url.endswith('/'):
            yield url


@source
def malbar():
    yield "https://www.malbar-saarbruecken.de/"
