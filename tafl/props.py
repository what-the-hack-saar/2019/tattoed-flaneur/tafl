from tafl.core import prop

#   Can be replace with jq-expressions
propertyPaths = {
  "Event": {
    "longitude":["location","geo","longitude"],
    "latitude":["location","geo","latitude"]
  },
  "Place": {
    "longitude":["geo","longitude"],
    "latitude":["geo","latitude"]
  }
}

def resolve(object, property):
    try:
        type = object["@type"]
        if not type in propertyPaths:
            return
        path = propertyPaths.get(type, {}).get(property, [])
        for property in path:
            object = object[property]
        return object
    except KeyError:
        return None


@prop
def name(entity_id, entity, cur):
    name = entity.get('name')
    if name:
        cur.execute("""
            insert into names (id, name) values (%s,%s)
            on conflict (id)
                do update set name = excluded.name
        """, (entity_id, name))


@prop
def coordinates(entity_id, entity, cur):
    longitude = resolve(entity, "longitude")
    latitude = resolve(entity, "latitude")
    if longitude and latitude:
        print("long:",longitude)
        print("lat:", latitude)
        cur.execute("""
            insert into coordinates (id, coord) values (%s,ST_Point(%s, %s))
            on conflict (id)
                do update set coord = excluded.coord
            """,
            (entity_id, longitude, latitude)
        )
