import requests
from bs4 import BeautifulSoup
import json
import itertools
from collections.abc import Mapping



def load_screening_data(url):
    response = requests.get(url)
    soup = BeautifulSoup(response.content, 'html.parser')

    for s in soup.find_all('script'):
        script = s.text.strip()
        if script.startswith('var programm'):
            return json.loads(script.split('=', 1)[1][:-1])

def extract_screenings(data):
    for movie in data['filme'].values():
        titel = movie['filmfakten']['titel']
        termine = movie['vorstellungen']['termine'].values()
        for t in termine:
            if isinstance(t, Mapping):
                yield titel, t
            else:
                for x in t:
                    yield titel, x


data = load_screening_data('https://www.camerazwo.de/programm')
for title, date in extract_screenings(data):
    print(title, date)
