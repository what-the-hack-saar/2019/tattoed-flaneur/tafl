create table entities (
    id serial primary key,
    source_url_id serial,
    url varchar(1000),
    type varchar(100),
    data jsonb
);

create unique index idx_entites_url_and_source on entities(source_url_id, url);


create table urls (
    id serial primary key,
    url varchar(1000)
);

create unique index idx_urls_url on urls(url);



create table coordinates (
    id serial,
    coord geometry(POINT)
);

create unique index idx_coordinates_id on coordinates(id);


create table names (
    id serial,
    name varchar(200)
);

create unique index idx_names_id on names(id);
