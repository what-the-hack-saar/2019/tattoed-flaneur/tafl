# -*- coding: utf-8 -*-

"""Console script for tafl."""
import sys
import click
import importlib
import logging

import psycopg2
import psycopg2.extras
import postgis.psycopg 
import coloredlogs

import tafl.core
import tafl.db

log = logging.getLogger(__name__)

#   Will go to some config file
extractors = [
    'tafl.sources',
    'tafl.encodings',
    'tafl.props'
]

con = None

@click.group()
def cli():
#    logging.basicConfig(level=logging.DEBUG)
    coloredlogs.install(level='DEBUG')

    global con
    con = psycopg2.connect(
        host='localhost',
        user='tafl',
        password='tafl',
        database='tafl')
    postgis.psycopg.register(con)
    psycopg2.extensions.register_adapter(dict, psycopg2.extras.Json)

    for e in extractors:
        importlib.import_module(e)


#   db


@cli.group()
def db():
    pass

@db.command()
def init():
    tafl.db.init(con)

@db.command()
def delete():
    tafl.db.delete(con)

@db.command()
def reset():
    tafl.db.delete(con)
    tafl.db.init(con)


#   urls


@cli.group()
def urls():
    pass

@urls.command()
def collect():
    tafl.core.collect(con)

@urls.command()
def crawl():
    tafl.core.crawl(con)


if __name__ == "__main__":
    sys.exit(cli())  # pragma: no cover
