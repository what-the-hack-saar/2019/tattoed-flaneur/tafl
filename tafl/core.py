import requests
from io import BytesIO
from bs4 import BeautifulSoup
import json
import itertools
import logging

log = logging.getLogger(__name__)

class Registry:
    def __init__(self):
        self.items = []

    def __call__(self, fkt):
        self.items.append(fkt)

    def __iter__(self):
        yield from self.items


#   Registries

sources = source = Registry()

encodings = encoding = Registry()

props = prop = Registry()


#   Main collect method, called by the cli tool.

def collect(con):
    cur = con.cursor()
    log.debug("Registered sources: {}".format([s.__name__ for s in sources]))
    for source in sources:
        log.info("Collecting source {}".format(source.__name__))
        for url in source():
            cur.execute("""
                insert into urls (url) values (%s)
                on conflict do nothing
            """, (url,))
            if cur.rowcount:
                log.info("New url: {}".format(url))
            else:
                log.debug("Existing url: {}".format(url))
    con.commit()


def _urls_to_crawl(con):
    cur = con.cursor()
    cur.execute("select * from urls")
    yield from cur


def crawl(con):
    print(props.items)
    print(sources.items)
    print(encodings.items)
    cur = con.cursor()
    for source_id, page in _urls_to_crawl(con):
        print(page)
        response = requests.get(page)
        soup = BeautifulSoup(response.content, 'html.parser')
        for e in encodings:
            for entity in e(soup, response):
                url = entity.get('url')
                type = entity.get('@type')

                print(url,type)

                if not url or not type:
                    #   ignore those for now
                    continue

                cur.execute("""
                insert into entities (source_url_id, url, type, data) values
                (%s, %s, %s, %s)
                on conflict (source_url_id, url) do update
                    set type = excluded.type, data = excluded.data
                returning id""", (source_id, url, type, entity))

                entity_id = cur.fetchone()[0]

                for p in props:
                    p(entity_id, entity, cur)
    con.commit()
