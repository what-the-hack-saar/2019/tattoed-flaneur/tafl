import logging
import os.path

log = logging.getLogger(__name__)

here = os.path.split(__file__)[0]

def load_sql(name):
    with open(os.path.join(here,'sql',name)) as f:
        return f.read()

def run_sql(con, name):
    sql = load_sql(name)
    cur = con.cursor()
    cur.execute(sql)
    con.commit()


def init(con):
    log.info("Initializing database structure.")
    run_sql(con, 'init-core-tables.sql')


def delete(con):
    log.info("Deleting database structure.")
    run_sql(con, 'delete-core-tables.sql')

