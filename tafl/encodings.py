import json
from tafl.core import encoding

@encoding
def json_ld(soup, response):
    for script in soup.find_all("script", attrs={'type':'application/ld+json'}):
        yield json.loads(script.text)
