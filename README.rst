==================================
Tattooed Flaneur command line tool
==================================

* Free software: GNU General Public License v3



data from https://download.geofabrik.de/europe/germany/saarland.html
Download https://download.geofabrik.de/europe/germany/saarland-latest.osm.pbf

create database tafl;
create user tafl with password 'tafl';
grant all on database tafl to tafl;

connect to database: \c tafl

create extension postgis;
create extension hstore;

sudo apt-get install osm2pgsql
-> v0.94.0

osm2pgsql --slim --hstore -W --host localhost --username tafl --database tafl saarland-latest.osm.pbf

Installation
------------

* You need an empty Postgresql (https://www.postgresql.org/) database. Version 10 or 11 should be fine.
* You need to install PostGIS (https://postgis.net/). The extension has to be installed on your system,
  but be aware that it also has to be activated in each database. You need admin rights to do so.
* Clone this repository. It is a good idea, to create a new virtual env, but it is not required.
* Install this library via `pip install -e .`.
* Check if `tafl --help` is working. If yes, you're ready to go!


Usage
-----




Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
